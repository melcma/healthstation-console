import React from 'react';

const Query = (props) => {

    return (
        <li className="analyzer__box  analyzer__website">
            {props.children}
        </li>
    );

};

export default Query;