import React from 'react';

const Prompt = (props) => {

    const checkQuery = (event) => {

        const target = event.target;
        const length = target.value.length;

        if (length > 1) {
            addQuery(target.value);
            clearInput(target);
        } else {
            console.log('nope');
        }
    };

    const addQuery = (el) => {
        props.addQuery(el);
    };

    const clearInput = (el) => {
        el.value = '';
    };

    return (
        <input
            onBlur={(e) => {
                checkQuery(e)
            }}
            className="analyzer__box  analyzer__prompt" placeholder="Enter url to check status"/>
    );

};

export default Prompt;
