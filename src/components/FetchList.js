import React from 'react';

const FetchList = (props) => {

    return (
        <ul className="analyzer__list">
            {props.children}
        </ul>
    );

};

export default FetchList;