import React, {Component} from 'react';
import './App.css';

class App extends Component {

    constructor() {
        super();

        this.state = {
            urls: [
                'www.adrianpiwowarczyk.com',
                'www.zizzi.co.uk',
                'www.nojolondon.co.uk',
                'www.grandpigalle.com',
                'www.kanada-ya.com',
                'www.goodmanrestaurants.com',
                'www.yahoo.com',
                'www.test.com',
                'www.rawifwjafoiajwfaw.com'
            ],
            websites: {}
        };

    };

    componentWillMount() {

        const newState = {...this.state};

        this.state.urls.map((result) => {
            newState.websites[result] = {
                https: '?',
                status: '000',
                time: '9.999'
            };
            return newState;
        });

        this.setState(newState);

    }

    componentDidMount() {

        this.state.urls.map((url) => {
            const fetchPromise = fetch('http://adrianpiwowarczyk.com:8002/logs/' + url)
                .then(response => response.json())
                .then((response) => {
                    const results = {};
                    results[url] = response;

                    const newState = {...this.state};

                    for (let key in results) {
                        newState.websites[key] = results[key];
                    }

                    this.setState(newState);


                    return results;
                });

            return fetchPromise;

        });

    }

    updateWebsites () {

        this.state.urls.map((url) => {
            const fetchPromise = fetch('http://adrianpiwowarczyk.com:8002/analyze/' + url)
                .then(response => response.json())
                .then((response) => {
                    const results = {};
                    results[url] = response;

                    const newState = {...this.state};

                    for (let key in results) {
                        newState.websites[key] = results[key];
                    }

                    this.setState(newState);


                    return results;
                });

            return fetchPromise;

        });
    }

    render() {
        return (
            <div className="analyzer">
                <table className="table">
                    <tbody>
                    <tr className="table__row">
                        <th className="table__cell">Domain</th>
                        <th className="table__cell">Https</th>
                        <th className="table__cell">Status</th>
                        <th className="table__cell">Time</th>
                    </tr>
                    {Object.keys(this.state.websites).map((website) => {
                        let results = this.state.websites[website];
                        return (
                            <tr key={website} className="table__row">
                                <td className="table__cell"><a href={"http://" + website} target="_blank">{website}</a></td>
                                <td className={results.https === true ? "table__cell" : "table__cell  table__cell--error"}>{results.https.toString()}</td>
                                <td className={results.status === 200 ? "table__cell" : "table__cell  table__cell--error"}>{results.status}</td>
                                <td className={results.time < 1 ? "table__cell" : "table__cell  table__cell--error"}>{results.time}s</td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
                <button onClick={() => { this.updateWebsites() }} className="btn  btn--update">Update</button>
            </div>
        );
    }
}

export default App;
